#!/usr/bin/env python
"""
@date 2013-07-23
@author Eugene Yurtsev
"""
import os, re
import numpy
from StringIO import StringIO

def parse_measurement(lines):
    """
    Returns a numpy ndarray from lines (with a potential header called 'Iteration')
    """
    if lines[0].strip().startswith('Iteration'):
        lines = lines[1:]

    if len(lines) == 0:
        data = numpy.nan
    else:
        s = StringIO(''.join(lines))
        data = numpy.genfromtxt(s)
    return data

def parse_plate_section(lines):
    """
    Inputs
    ---------
    lines -- list of strings belonging to a single "PLATE" measurement section
    (Could be a single measurement or else multiple measurements)

    Returns
    ---------
    measurements : list
        each element is a dictionary with the following keys:
            * wavelength (str)
            * data (ndarray)
    """
    ##
    # Extract the name of the measurement using a regular expression
    # the first line must encode the name of the measurement

    wavelength = re.search('(?<=Wavelength:)[0-9/]+', lines[0]).group()

    inds = [i for i, line in enumerate(lines) if line.strip().startswith('Iteration')]

    if len(inds) == 0:
        inds = [1] # only one measurement is present

    measurements = []

    for i, start in enumerate(inds):
        if i == len(inds) - 1:
            end = len(lines)
        else:
            end = inds[i+1]
        data = parse_measurement(lines[start:end])
        measurements.append({'wavelength' : wavelength, 'data' : data})

    return measurements

def parse_file(path):
    """
    Parse a plate reader output text file.
    Returns a list of measurements ordered as they're found in the file.
    Each measurement is a dictionary containing the wavelength (wavelength)
    and data (numpy ndarray).

    Returns
    -----------
    measurements : list
        Each measurement is a dictionary composed of 3-keys:
            * wavelength - a string describing which wavelength were used for measuring
            * data - numpy ndarray containing the results of the measurement
    """
    with open(path) as f:
        lines = f.readlines()

    lines = filter(lambda l: len(l.strip()), lines)
    lines = filter(lambda l: not l.strip().startswith('Results'), lines)
    # Remove names of measurements for the time being (For simplicity of writing the parser.

    # Find indices of new measurement lines
    inds = [i for i, line in enumerate(lines) if line.strip().startswith('Plate')]
    measurements = []
    for i, start in enumerate(inds):
        if i == (len(inds) - 1):
            end = len(lines)
        else:
            end = inds[i+1]

        section_measurements = parse_plate_section(lines[start:end])
        measurements.extend(section_measurements)

    return measurements

if __name__ == '__main__':
    import glob
    #file_names = glob.glob('../tests/data/varioskan/*.txt')
    fname = '../tests/data/Varioskan/Format_05.txt'
    a = parse_file(fname)

