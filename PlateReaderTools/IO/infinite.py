#!/usr/bin/env python
import numpy as np
import re
from StringIO import StringIO

def parse_measurement(lines):
    """
    Parses a segment of lines that describes a single measurement.
    Returns a dictionary containing the meta data that describes the measurement
    """
    measurement= {}
    meta = {}
    meta['time'] = float(lines[0].split('s')[0]) / 3600.0
    meta['temperature'] = float(lines[1].split(' ')[0])
    #measurement['columns'] = lines[2].split('\t')[1:13] # temporary hard coded
    #measurement['rows'] = row_labels = [l[0] for l in lines[3:]] # hard coded

    # Parse the data
    lines = [l.replace('\t\t', '\tnan\t').replace('\t\t', '\tnan\t') for l in lines] # Need to do twice. Why?

    data = ''.join([l[1:] for l in lines[3:]])

    s = StringIO(data)
    measurement['data'] = np.genfromtxt(s, missing_values='nan', filling_values=np.nan)
    measurement['meta'] = meta

    return measurement

def parse_wavelength_from_meta(lines):
    """ TODO: FIX THIS TO BE ROBUST. What are other measurements reported as? """
    lines = [line for line in lines if 'Measurement Wavelength' in line]
    wavelengths = [[s for s in line.split() if s.isdigit()] for line in lines]

    if len(wavelengths) > 1:
        return 'Unknown'

    if len(wavelengths[0]) > 1:
        return 'Unknown'

    return wavelengths[0][0]

def parse_file(path):
    '''
    Parse a plate reader output text file.
    Returns a list of measurements ordered as they're found in the file.
    Each measurement is a dictionary containing the measurement ID and data.
    The data in each measurement is itself a dictionary keyed by the measurement wavelength.
    Note that measurement names are not always unique!
    '''
    with open(path) as f:
        lines = f.readlines()

        # Remove emptylines
        lines = filter(lambda l:len(l.strip()), lines)

        # Identify where the meta and data sections begin
        for index, line in enumerate(list(lines)):
            match = re.search('[a-zA-Z]{2}', line) # Assumes meta begins when a line with 2 letters is encountered.
            if index <> 0 and match: # Meta does not start on the first line. This is a dirty that fixes a problem with a few formats.
                data_section = lines[:index]
                meta_section = lines[index:]
                break

        wavelengths = parse_wavelength_from_meta(meta_section)

        ###
        # Find indices of new measurement lines
        # -2 because there is a temperature and time information that starts before the <> mark
        inds = [i-2 for i, line in enumerate(data_section) if line.startswith(r'<>')]

        measurements = []
        for i, start in enumerate(inds):
            if i == len(inds) - 1:
                end = len(data_section)
            else:
                end = inds[i+1]

            measurement = parse_measurement(data_section[start:end])
            measurement.update({'ID' : (i + 1), 'wavelength' : wavelengths})
            measurements.append(measurement)

        ###
        # Parse the meta data
        meta_data = meta_section

    return measurements #meta_data, measurements

if __name__ == '__main__':
    import glob
    #fname = '../tests/data/Infinite/sample.asc'
    fname = '../tests/data/Infinite/sample_03.asc'
    #print glob.glob('../tests/data/infinite/*.asc')
    meta, measurements = parse_file(fname)
    #print measurements

