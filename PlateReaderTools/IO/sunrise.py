#!/usr/bin/env python 
"""
Parser for the sunrise plate reader
"""
import matplotlib.cm as cm
import matplotlib.pyplot as plt


import numpy
import re, os, time, glob
#from datetime import datetime

try:
    from BeautifulSoup import BeautifulSoup
    beautiful_soup_imported = True
except ImportError as e:
    beautiful_soup_imported = False

class SunriseMeasurementParser():
    def __init__(self, xmlsoup):
        """ Feed in an XML measurement. """
        self.xmlsoup = xmlsoup
        self.read_meta()
        self.read_data()

    #def getTimeStamp(self):
        #timeStamp = datetime.strptime(self.meta['time string'][:-3], "%Y%m%d%H%M%S")
        #return timeStamp

    def read_meta(self):
        self.meta = {}
        #self.meta['time'] = self.xmlsoup.get('time')
        #self.meta['time string'] = self.xmlsoup.time.string
        self.meta['date'] = self.xmlsoup.get('date')
        self.meta['time'] = self.xmlsoup.header.time.string
        self.meta['wavelength'] = self.xmlsoup.absorbance.get('wavelength')
        self.meta['data type'] = self.xmlsoup.dataset.get('type')
        self.meta['info'] = self.xmlsoup.info.string

        try:
            self.meta['barcode'] = self.xmlsoup.barcode.string
        except:
            self.meta['barcode'] = ''

    def read_data(self):
        """
        Extracts the data from the xml soup and formats it into a numpy matrix.
        """
        self.data = numpy.nan * numpy.zeros((8,12))
        xml_wells = self.xmlsoup.dataset.findAll('well')

        for well in xml_wells:
            well_ID = well.get('pos')
            row = ord(well_ID[0]) - ord('A');
            col = int(well_ID[1:]) - 1
            self.data[row, col] = float(well.string)

    def __str__(self):
        return 'Wavelength {0}\nData:\n{1}'.format(self.meta['wavelength'], self.data)

    def __repr__(self):
        return 'Wavelength {0}\nData:\n{1}'.format(self.meta['wavelength'], self.data)

def parse_file(path, **kwargs):
    """
    Parse files from the sunrise plate reader.
        Note: This parser makes the use of the beautiful soup library.
    Returns a list of measurements ordered as they're found in the file.
    Each measurement is a dictionary containing the measurement name and data.
    The data in each measurement is itself a dictionary keyed by the measurement wavelength.
    Note that measurement names are not always unique!

    Returns
    -----------
    a 2-tuple : meta_data, measurements
        meta_data : dict
            meta data that applies to the whole plate (rather than to single measurements)
            e.g., the date of the measurement, the protocol used, etc.
            The output depends on whatever meta data is available inside the file.
        measurements : list
            A list of measurements.
            Each measurement is a dictionary composed of 3-keys:
                ID - This is the ID or name of the measurement (Not guaranteed to be unique)
                wavelengths - a string describing which wavelengths were used for measuring
                data - numpy ndarray containing the results of the measurement
    """
    if not beautiful_soup_imported:
        raise Exception('You must install the BeautifulSoup python package in order to read data from the sunrise plate reader.')

    with open(path, 'r') as finput:
        xmlsoup = BeautifulSoup(finput.read())

    measurement_list = [SunriseMeasurementParser(m) for m in xmlsoup.findAll('measurement')]

    measurements = [{'ID' : m.meta['time'], 'wavelengths' : m.meta['wavelength'], 'data' : m.data, 'meta' : m.meta} for m in measurement_list]
    meta_data = {}

    return meta_data, measurements

if __name__ == '__main__':
    """ Harmless test code. """
    import glob

    files = glob.glob('/home/eyurtsev/pyrep/PlateReaderTools/tests/data/Sunrise/*.XML')

    print parse_file(files[0])

