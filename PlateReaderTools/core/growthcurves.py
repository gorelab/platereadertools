#!/usr/bin/env python
# a module that helps to fit growth curves vs. time and plot them
# Module requires clean up before it can be used
import numpy
from numpy import arange, array, polyfit, mean, log, isnan, nan
import matplotlib.pyplot as plt
import glob
from GoreUtilities import graph, util
import string
from scipy import optimize

def extract_index_of_threshold(time_series, threshold):
    """
    Returns the first index when the time series goes above a given threshold

    Parameters
    -----------
    time_series : ndarray
    threshold : int | float

    Returns
    --------
    Index of first index if found, nan otherwise
    """
    indexes = numpy.where(time_series > threshold)[0]
    if len(indexes) > 0:
        return indexes[0]
    else:
        return numpy.nan

def interpolate_time_of_cross(time_series, threshold, window_len=3, kind='log'):
    """
    Returns the first index when the time series goes above a given threshold

    Parameters
    -----------
    time_series : ndarray
    threshold : int | float
    window_len : int
        Length of window around the threshold to use for interpolation
    kind : 'log' | 'linear'
        Fits a straight line through either the log of the data or the raw data ('linear')

    Returns
    --------
    Index of first index if found, nan otherwise
    """
    # Calculate index around which to interpolate    
    iindex = extract_index_of_threshold(time_series, threshold)

    if iindex is nan:
        return nan

    # Extract the relevant data segment
    x = range(window_len)
    offset = -int(window_len)/2
    y = time_series[(iindex+offset):(iindex+offset+window_len)]

    # Log transform y, so we can fit a line through it.
    if kind == 'log':
        y = log(y)
        threshold = log(threshold)

    pars = polyfit(x, y, 1)
    ##
    # Solve the equation y = ax + b for x when y is known (y=threshold)
    #
    # pars[0] is the slope, pars[1] is the intercept
    #
    xcross = (threshold - pars[1]) / pars[0]

    return iindex + xcross + offset

def extract_fold_growth_in_range(time_series, window_len, lt, ht, dtime=None):
    """
    Parameters
    ---------------
    time_series : 1d array

    window_len : int
        the number of consecutive points to use for fitting a slope to

    lt : float
        low threshold
    ht : float
        high threshold

    dtime : None | float
        if provided the time is renormalized by dtime.
    """
    #data = time_series
    #ilow = extract_index_of_threshold(data, lt)
    #ihigh = extract_index_of_threshold(data, ht)
#
    #if max(data) < ht:
        #ihigh = 'end'
#
    #if isnan(ilow) or ihigh is nan:
        #return nan, nan, nan, nan
#
    #if ihigh == 'end':
        #data = data[ilow:]
    #else:
        #data = data[ilow:ihigh]
#
    # TODO Simply use density returned by extract_fold_growth for this.
    raise NotImplementedError
    return extract_fold_growth(data, window_len, delta_time=dtime)

def extract_fold_growth(time_series, window_length, delta_time=None, lt=None):
    """
    Calculates instantaneous fold growth (1/N * dN/dt) for a time series.

    Parameters
    ---------------
    time_series : 1d array

    window_length : int
        length of window to use for fitting a line

    delta_time : float
        time between consecutive measurements

    lt : float
        if provided, then sets fold_growth to nan
        if any of the data points in the data segment
        are smaller than lt

    Returns
    ---------
    3-tuple

    1st element:
        list of fold_growth

    2nd element:
        const from fit

    3rd element:
        mean density at each point

    # TODO add time as output
    """
    time = arange(window_length)
    segments = map(lambda x : time_series[x:(x+window_length)],
                        arange(len(time_series)-window_length+1))

    ## Using log

    fit_array = array(map(lambda x : polyfit(time, log(x), 1), segments))
    density = array(map(lambda x : mean(x), segments))

    #density = mean(array(segments), axis=1)#harray(map(lambda x : mean(x), segments))

    fold_growth, const = fit_array[:, 0], fit_array[:, 1]

    if delta_time is not None:
        fold_growth = fold_growth / delta_time

    if lt is not None:
        invalid_indexes = array(map(lambda x : min(x) < lt, segments))

        if any(invalid_indexes):
            fold_growth[invalid_indexes] = nan
            const[invalid_indexes] = nan

    return density, fold_growth, const

######################################
# Models to use for growth curves 
######################################

def fit_tf_exp_growth_with_lag_phase(g, tlag, Nt, Ni):
    """
    Simplest growth model: lag phase followed by exponential growth.
    g = growth_rate
    Nt = threshold density
    Ni = initial density
    """
    return numpy.log(Nt/Ni) / g + tlag

def residuals_fit_tf_exp_growth_with_lag_phase(pars, fixed_pars):
    """
    Residuals
    """
    g, tlag = pars
    tf = fixed_pars['tf']
    Nt = fixed_pars['Nt']
    Ni = fixed_pars['Ni']

    return tf - fit_tf_exp_growth_with_lag_phase(g, tlag, Nt, Ni)

class ExtractGrowthParameters(object):
    def __init__(self, Ni, Nt, tf):
        """
        Use to extract strain parameters from an overnight run.
        What is needed are time series where the strain is started at different initial densities and is
        grown at the same conditions.
        """
        self.Ni, self.tf = Ni, tf
        self.Nt = Nt
        self.fit_growth()
    def fit_growth(self):
        try:
            Ni, tf = util.remove_nans(self.Ni, self.tf)
            fixed_pars = {'Ni' : Ni, 'tf' : tf, 'Nt' : self.Nt}
            A = optimize.leastsq(residuals_fit_tf_exp_growth_with_lag_phase,
                        (1.0, 2), args=(fixed_pars, ), full_output=True)
            self.pars = A[0]

        except TypeError as e:
            self.pars = numpy.nan, numpy.nan
        return self.pars

    def plot_data(self, annotate=True, **kwargs):
        x = numpy.log(self.Nt/self.Ni)
        y = self.tf
        lines = plt.plot(x, y, **kwargs)

        if annotate:
            plt.xlabel('log(Nt/Ni)')
            plt.ylabel('Time to Threshold')
        return lines

    def plot_fit(self, annotate=True, **kwargs):
        x = numpy.log(self.Nt/self.Ni)
        y = fit_tf_exp_growth_with_lag_phase(self.g, self.tlag, self.Nt, self.Ni)
        lines = plt.plot(x, y, **kwargs)

        if annotate:
            plt.xlabel('log(Nt/Ni)')
            plt.ylabel('Time to Threshold')
        return lines

    def plot_residuals(self, annotate=True, **kwargs):
        x = numpy.log(self.Nt/self.Ni)
        lines = plt.plot(x, self.residuals, **kwargs)
        if annotate:
            plt.xlabel('log(Nt/Ni)')
            plt.ylabel('Diff tf (real vs. fit)')
        return lines

    @property
    def residuals(self):
        x = numpy.log(self.Nt/self.Ni)
        yfit = fit_tf_exp_growth_with_lag_phase(self.g, self.tlag, self.Nt, self.Ni)
        yreal = self.tf
        return yreal - yfit

    @property
    def g(self):
        return self.pars[0]

    @property
    def tlag(self):
        return self.pars[1]


#######################
# REFACTOR CODE BELOW
#######################

def plot_instantaneous_growth_rate(well, window_len, dtime, lt=None, annotate=False, **kwargs):
    slope_pars = extract_fold_growth(well.data, window_len, delta_time=dtime, lt=lt)
    plt.plot(slope_pars[0], slope_pars[1], **kwargs)
    if annotate:
        plt.ylabel('Instantaneous growth rate (1/hr)')
        plt.xlabel('Instantaneous population density (OD)')


def plot_growth_curve(time, value, plot_fit_pars=True, plot_fit_region=False, fit_dictionary=None, label_plot=False, ax=None, **kwargs):
    """
    Plots the value (for example OD) vs. time (optional: includes fit information)
    """
    kwargs.setdefault('marker', 'o')
    kwargs.setdefault('color', 'k')
    kwargs.setdefault('markersize',1)
    #kwargs.setdefault('linestyle', None)

    if ax is not None:
        plt.sca(ax)
    else:
        ax = plt.gca()

    if fit_dictionary is not None:
        ftime = fit_dictionary['fit time']
        time_offset = fit_dictionary['fit time offset']
        plot_func = fit_dictionary['plot function']
        fit_pars = fit_dictionary['fit output']

        plt.plot(ftime+time_offset, plot_func(ftime, *fit_pars[0]), '-b', linewidth=3, alpha=0.5) # Plot fit

        if plot_fit_pars:
            plt.text(0.1, 0.9, fit_dictionary['fit text'], transform=plt.gca().transAxes, verticalalignment='top') # Plot fit parameters

    # Plots the data
    plt.plot(time, value, **kwargs)

    if plot_fit_region:
        plt.plot(fit_dictionary['fit time'] +fit_dictionary['fit time offset'], fit_dictionary['fit OD'], 'ok', markersize=2, linestyle='-') # For debugging if needed

    if label_plot:
        plt.xlabel('Time (hours)')
        #plt.ylabel('OD')

    return ax

def fit_growth_curve(tTime, OD,  time_lim=None, OD_lim=None, fit_type='fit exponential growth', initial_pars=None):
    """ Fits a function to the growth curve.
    Assumes that the background has been already subtracted from the OD measurement.
    time is measured in hours
    OD can be in any units
    fitIndexes can specify which time indexes to fit to.
    fitType specifies what kind of fit to apply to the data.

    returns None in case of failure. (TODO: refine explanation)
    returns the a dictionary with fit properties
    """
    fit_dictionary = {}

    fit_indexes = get_fit_indexes(tTime, OD, time_lim, OD_lim)

    ftime = tTime[fit_indexes] # ftime = fit_time -> the region of the time to fit
    fOD = OD[fit_indexes] # fit OD = the region of the OD to fit

    if len(ftime) == 0:
        return None

    time_offset = ftime[0] # figure out what the time offset is for fitting
    ftime = ftime - ftime[0] # Shift the time for the fits...

    if fitType == 'fit logistic with time lag':
        fit_func, plot_func = ED.fit_logis_with_lag, ED.logis_with_lag

        if initial_pars is None:
            initialPars = (1.1, 20000.0, 300.0, 1)

        key_list = ['r', 'K', 'y0', 'time lag']

        fit_dictionary['fit text'] = 'Logistic growth with time lag\nr : {r:.2f}/hr\nK : {K:.2f}\ny0 : {y0:.2f}\ntime lag = {time lag:.2f} hr'

    elif fitType == 'fit exponential growth':
        fit_func, plot_func  = ED.fit_exp_growth, ED.exponential_growth

        if initial_pars is None:
            initial_pars = (1.3, 1)

        key_list = ['r', 'y0']

        fit_dictionary['fit text']  = 'Exponential growth\nr : {r:.2f}/hr\ny0 : {y0:.2f}'

    elif fit_type == 'fit exponential growth with time lag':
        fit_func, plot_func = ED.fit_exp_growth_with_lag, ED.exponential_growth_with_lag

        if initial_pars is None:
            initial_pars = (1.3, 1, 1)

        key_list = ['r', 'y0', 'time lag']

        fit_dictionary['fit text']  = 'Exponential growth with time lag\nr : {r:.2f}/hr\ny0 : {y0:.2f}\ntime lag = {time lag:.2f} hr'
    else:
        util.raiseWithMessage('fit type not defined')

    try:
        fit_pars = fit_func(ftime, fOD, *initialPars)
    except:
        return None
    fit_dictionary.update({this_key : fit_pars[0][index] for index, this_key in enumerate(key_list)})
    fit_dictionary['fit text'] = fit_dictionary['fit text'].format(**fit_dictionary)
    fit_dictionary['plot function'] = plot_func
    fit_dictionary['fit function'] = fit_func
    fit_dictionary['fit type'] = fit_type
    fit_dictionary['fit output'] = fit_pars
    fit_dictionary['fit time'] = ftime
    fit_dictionary['fit time offset'] = time_offset
    fit_dictionary['fit OD'] = fOD

    return fit_dictionary

def get_fit_indexes(tTime, OD, time_lim=None, OD_lim=None):
    """
    """
    fit_indexes = numpy.ones(numpy.shape(OD), dtype=bool)

    if time_lim is not None:
        fit_indexes = numpy.array([this_time > time_lim[0] and this_time < time_lim[1] and fit_indexes[index] for index, this_time in enumerate(tTime)])

    if OD_lim is not None:
        fit_indexes = numpy.array([thisOD > OD_lim[0] and thisOD < OD_lim[1] and fit_indexes[index] for index, thisOD in enumerate(OD)])

    return fit_indexes

def fit_overnight_plate(time, OD, time_lim=None, OD_lim=None, fit_type='fit exponential growth', initial_pars=None):
    """
    """
    numRows, numCols = OD[0].shape

    fit_dictionary_matrix = [[numpy.nan for col in range(numCols)] for row in range(numRows)]

    for plate_row in range(numRows):
        for plate_col in range(numCols):
            mTime = numpy.array(FlyingButterfly.trackDataOverTime(time, plateRow, plateCol))
            mPop = numpy.array(FlyingButterfly.trackDataOverTime(OD, plateRow, plateCol))
            fit_dictionary_matrix[plateRow][plateCol] = fitGrowth_curve(mTime, mPop, time_lim=time_lim, OD_lim=OD_lim, fit_type=fit_type, initial_pars=initial_pars)

    return fit_dictionary_matrix

def plot_time_course_plate(time, OD, fit_dictionary_matrix=None, xlim=None, ylim=None, **kwargs):
    """
    Documentation is out-dated.
    TODO: Write documentation
    Input:
        3D time matrix. Entries correspond to the times in which each well was measured.
        The first dimension containes different measurements on the same plate.
        The next two dimensions specify which well was measured.

        i.e., the time at which well A8 was measured during the 15th measurement would be: time[14, 0, 7]

        OD -> 3d OD matrix. Same interpretation as the time matrix.

        fitDictionaryMatrix - specifies how to plot the fit data. This is a 2D matrix where each entry contains the output of the fitGrowthCurve function.

    OD should be corrected for noise already.
    time should be in whatever units it needs to be. (hours).
    """
    row_num, col_num = OD[0].shape
    ax_main, ax_matrix = graph.create_grid_layout(rowNum=row_num, colNum=col_num, xlim=xlim, ylim=ylim, row_labels = string.ascii_uppercase, col_labels=range(1, col_num+1))

    if isinstance(time, list):
        time = numpy.array(time)
    if isinstance(OD, list):
        OD = numpy.array(OD)

    for (row, col), ax in numpy.ndenumerate(ax_matrix):
        plt.sca(ax)

        if fit_dictionary_matrix is not None:
            fit_dictionary = fit_dictionary_matrix[row][col]
        else:
            fit_dictionary = None

        plot_growth_curve(time[:, row, col], OD[:, row, col], plot_fit_pars=False, plot_fit_region=False, fit_dictionary=fit_dictionary, **kwargs)

    plt.autoscale()
    plt.sca(ax_main)

    return ax_main, ax_matrix

def overnight_analysis(filepath, action='plot raw', fit_type='fit exponential growth', OD_lim=(0, 4000), time_lim=(0, 5), ylim=None, autolabel=True, OD_Background=520.00):
    time, OD, plateList = load_overnight(filepath)
    time = time / 60.00
    OD = OD - OD_Background

    if fitType is not None:
        fit_matrix = fit_overnight_plate(time, OD, fit_type=fit_type, OD_lim=OD_lim, time_lim=time_lim)
    else:
        fit_matrix = None

    numRows, numCols = OD[0].shape

    if action=='plot raw':
        fig = plt.gcf()

        plot_overnight_plate(time, OD, fit_dictionary_matrix=fit_matrix, ylim=ylim)

        if autolabel:
            ax = fig.add_subplot(numRows, numCols, 85)
            FlyingButterfly.hideAxes(ax, hide=False)

            ax.set_xlabel('Time (hours)')
            ax.set_ylabel('OD')

        if autolabel:
            ax_main1 = fig.add_subplot(111)
            FlyingButterfly.hideAxes(ax_main1)

    elif action=='plot fit pars':
        fig = plt.figure()

        fit_par = numpy.zeros((numRows, numCols)) * numpy.nan

        if fit_matrix is not None:
            for row in range(numRows):
                for col in range(numCols):
                    if fit_matrix[row][col] is not None:
                        fit_par[row, col] = fit_matrix[row][col]['r']

        FlyingButterfly.plotHeatMap(fit_par, include_values=True)

    return fitMatrix

