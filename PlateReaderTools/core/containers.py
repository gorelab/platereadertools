#!/usr/bin/env python
# TODO: plate.data make sure data returned is homogenous ?
# TODO: Figure out how to handle plate shape
#
"""
REFACTORING THE UNDERLYING DATA STRUCTURE
--------------------------

Loading Data
-----------------

Plate.from_file :: file or list of files -> Returns Plate Container
Plate.from_dir :: directory, user provides pattern? -> Returns Plate Container.

How the data is organized
----------------------------

Most general case:

Plate.data query returns a DYNAMICALLY created 4dPanel:

Dimensions:
    1st - Columns (e.g., 1..12)
    2nd - Rows (e.g., A-H)
    3rd - wavelength (e.g., '600')
    4th - Iteration / Time Point (e.g., 5)

When there is only one measurement in the plate
Plate.data may end up returning just a DataFrame.

Requirements for parsers
---------------------------

A parser accepts a filepath and optional *args, **kwargs as parameters and returns:

a LIST of DICT items. The LIST is according to the order of measurements encountered in the file.
Each list item is a dictionary with:
    REQUIRED key : value pairs:
    * 'data' : 2d numpy matrix containing the values of the measurement.
    * 'wavelength' : string describing the type of the measurement.
        For example '600', would represent 600nm, absorbance. '520/540' represents fluorescence.
    * 'iteration' : # of measurement.
    * 'meta' : meta data about the measurement. For example, temperature, user, protocol, etc.

    OPTIONAL key : value pairs:
    'name' : optional name for the measurement. e.g., 'Eugenes OD measurement' <-- (But this should be present in the file.)
    'time' : mapping between iteration # to time that is meaningful.
        e.g., seconds after beginning of first measurement. Or else Exact Date + Exact time. ('2013/10/08' 23:00:01. Preferably in 24 hour format)


Underlying organization for data in the Plate Container (Is this necessary?)
-----------------------------------------------------------------------------

Rationale:
(1) The need for a measurement class is just to somehow help keep track of meta data alongside with the dataframe that describes data.
One way of doing it is to keep two linked DataFrames (one for data, the other for meta)

(2) The other way of restructing this is to have a DataFrame that contains WellObjects. I am not sure what the downsides to this is yet.

A plate contains a list of measurements. Each measurement is an instance of a MEASUREMENT class.

The MEASUREMENT CLASS defines the following API.
    plot() -> wrapper around plot heat map.
    values() -> returns the values
    exposes interface of dataframe.
    .data -> returns the dataframe
    .meta -> Returns meta data about the measurement.
    .apply -> Applies a transformation on a per well basis.

    Supports slicing according to well -> Returning a new measurement class with just that data present.

Slicing On Plate Container
----------------------------
All slicing operations return a plate object, with data reduced to exactly what is relevant.

Plate['A2'] -> Slices a given well
Plate.it[2] -> Slice at a given iteration ? Support mixed labeling for time in specific units?
Plate.iw['60'] -> Slice at a given wavelength
Plate.wavelengths ->  Return a list of the wavelength
Plate.ix -> Slice using ix of underlying pandas structure

Accessing underlying data
----------------------------

Plate.data -> returns pandas structure
Plate.values -> Returns pandas.structure.values


Plotting
-------------------

Plate.iw('600').plot(what='growth rate')
Plate.plot('600', what='value') <-- Automatically switches between heat_maps and
Plate.plot('600', what='slope') <-- only makes sense in the case of time_series
Plate.plot('600', what='growth rate') <-- only makes sense in the case of time_series

Calculations
----------------------

For time courses
=======================

    Plate.calc('600', what='slope', parameters={dictionary}) -> ??

    Returns data_frames.

    Plate.calc('600', what='time to reach threshold', how='interpolate')
    Plate.calc('600', what='time to reach threshold', how='discrete')
"""
from GoreUtilities import plot_heat_map, graph, BaseObject
from GoreUtilities.util import to_list, get_files, rotate_ndpanel_dimensions
import pylab as pl
import pandas
import string
import numpy
from numpy import arange
import matplotlib

class PRMeasurement(object):
    """
    PRMeasurement object.
    Defines convenient plotting routine for the data frame.
    May end up removing this later.
    """
    def __init__(self, wavelength, data, meta=None):
        self.wavelength = wavelength

        if isinstance(data, pandas.DataFrame):
            self.data = data
        else:
            row_num, col_num = data.shape
            row_labels = [string.ascii_uppercase[r] for r in range(row_num)]
            col_labels = [str(x) for x in numpy.arange(col_num) + 1]

            self.data = pandas.DataFrame(data, index=row_labels, columns=col_labels)

        self.meta = meta

    @classmethod
    def from_dict(cls, m_dict):
        return cls(m_dict.get('wavelength', numpy.nan), m_dict['data'], m_dict.get('meta', None))

    def __str__(self):
        return 'Wavelength: {0}\n{1}'.format(self.wavelength, self.data.__str__())

    @property
    def values(self):
        return self.data.values

    def plot(self, include_values=True, cmap=matplotlib.cm.gray,
                row_labels=None, col_labels=None, **kwargs):
        return plot_heat_map(self.data, include_values=include_values,
                xtick_labels=col_labels, ytick_labels=row_labels, cmap=cmap, **kwargs)

class Well(object):
    """
    A well instance is dynamically generated when indexing on the plate yields a single well.
    At the moment, this is just a wrapper around a DataFrame.
    """
    def __init__(self, ID, data=None, meta=None):
        self.ID = ID
        self.meta = meta
        self.data = data

    def plot(self, what=None, ax=None, **kwargs):
        """
        Default behavior would be to plot all the differently detected wavelength
        and plot them vs. time
        Also, looks whether there exists a time index. If yes then behavior with time as expected.
        """
        if what is None: what = self.data.columns
        if ax is None: ax = pl.gca()

        return self.data[what].plot(ax=ax, **kwargs)

    def __str__(self):
        return 'Well {0}\n{1}'.format(self.ID, self.data.__str__())

    @property
    def wavelengths(self):
        return self.data.columns

    @property
    def values(self):
        return self.data.values

class PRPlate(BaseObject):
    """
    A container for measurements carried on a plate.
    """
    def __init__(self, ID, datafiles=None, measurements=None, meta=None):
        """
        Container for plate reader measurements.

        Parameters
        ------------
        ID : any type
        datafiles : None | str | list of str
            paths to data files from which to read the measurements.
            if list of str then measurements will be loaded
            according to the order of the file paths in the list
        measurements : None | PRMeasurement | list of PRMeasurement
        meta : dictionary with meta data
        shape : 2-tuple
        """
        self.datafiles = to_list(datafiles)
        self.measurements = to_list(measurements)
        self.ID = ID
        self.meta = meta

        if self.measurements is not None:
            self._initialize_data_from_measurements()

    @classmethod
    def from_file(cls, ID, path, parser):
        """
        Builds a plate object from specified filepath(s) using the specified parser.

        Parameters
        ------------
        ID : str | integer
            This is an identifier that will be assigned to the plate.
            It's a good idea to make sure the identifier is unique.
        path: str | list of str
            path to a single data file or else a list of paths to data files
        parser : callable
            A callable that accepts a path and returns a 2-tuple (meta_data, data)
            (Note: Be careful to pass the correct parser for the given
            file type)
        Returns
        ---------
        An instance of plate object with the data loaded
        """
        self = cls(ID, path)
        self._read(parser)
        return self

    @classmethod
    def from_dir(cls, ID, path, parser, pattern='*.*', recursive=False):
        """
        Builds a plate object from specified filepath(s) using the specified parser.

        Parameters
        ------------
        ID : str | integer
            This is an identifier that will be assigned to the plate.
            It's a good idea to make sure the identifier is unique.
        path: str | list of str
            path to a single data file or else a list of paths to data files
        parser : callable
            A callable that accepts a path and returns a 2-tuple (meta_data, data)
            (Note: Be careful to pass the correct parser for the given
            file type)
        Returns
        ---------
        An instance of plate object with the data loaded
        """
        datafiles = get_files(path, pattern, recursive)
        datafiles.sort()
        return cls.from_file(ID, datafiles, parser)

    @classmethod
    def from_measurements(cls, ID, measurements, meta=None):
        """
        Constructs a plate container from a list of measurements
        """
        instance = cls(ID, measurements=measurements, meta=meta)
        return instance

    def _read(self, parser):
        """
        Reads the data from from the indicated file path using the indicated parser
        """
        self.meta_data = []
        self.measurements = []

        for file_path in self.datafiles:
            measurements = parser(file_path)
            self.measurements.extend([PRMeasurement.from_dict(m) for m in measurements])

        self._initialize_data_from_measurements()

    def __getitem__(self, key):
        """
        Supports picking a single well from the plate.

        plate[:] - iteration slicing
        plate['A1'] - choosing wells
        plate[:, :] - Wells

        TODO: Fix function non 3d arrays
              Figure out what to do with meta
        """
        if isinstance(key, (slice, int)):
            indexing = 'time, slice'
            key = to_list(key)

            data = self.data.iloc[key]
            shape = data.shape
            ndim = len(shape)

            if list(shape[:2]) == [1, 1]:
                # Edge case to support plate.iw('600')[0] 
                w = data.items[0]
                return PRMeasurement(w, data.squeeze())
            else:
                new = self.copy()
                new.data = data
                return new
        elif isinstance(key, tuple):
            ## plate[:, :] used for indexing positions on the plate only.
            kr, kc = key
            new = self.copy()
            data = new.data

            if isinstance(kr, str): kr = list(kr)
            if isinstance(kc, str): kc = list(kc)

            data = data.ix[:, :, kr, kc]

            if list(data.shape[-2:]) == [1, 1]:
                key = '{0}{1}'.format(data.major_axis[0], data.minor_axis[0])
                return self._get_well(key)
            else:
                new.data = data
                return new
        elif isinstance(key, str):
            return self._get_well(key)
        else:
            raise ValueError('Not a supported key format')

    def _get_well(self, key):
        """
        Provides a way to index positions.

        Parameters
        --------------
        key : str | 2-tuple of slices

        Returns
        --------------

        The output depends on the number of wells remaining at the end of slicing:

        > 1 well -> a plate container is returned.
        1 well -> Well instance

        Otherwise (TODO did not implement yet.)
        """
        if isinstance(key, str):
            # TODO FIX here to get all alpha from alphanumeric for row
            key = key.capitalize()
            row = filter(str.isalpha, key) # Gets the alpha part of the string
            col = filter(str.isdigit, key) # Gets the numberic part of the string

            rows   = self.data.major_axis
            columns = self.data.minor_axis

            try:
                row = list(rows).index(row)
                col = list(columns).index(col)
            except ValueError:
                raise ValueError('Well {0} does not exist in the plate.'.format(key))

            indexing = 'well, loc'

            data = rotate_ndpanel_dimensions(self.data, 2, 'right')
            data = data.iloc[row].iloc[col]
            return Well(key, data=data, meta=None)
        else:
            raise ValueError('Not a supported key format.')


    @property
    def wavelengths(self):
        """ Returns list of wavelengths. """
        return self.data.items

    @property
    def values(self):
        """ Returns the numerical values of all measurements. """
        return self.data.values

    def _initialize_data_from_measurements(self):
        """ Called to initialize self._data when constructing plate container from a file. """
        wavelengths = set([m.wavelength for m in self.measurements])

        d_by_w = {}

        for this_wavelength in wavelengths:
            d_by_w[this_wavelength] = []

        for m in self.measurements:
            d_by_w[m.wavelength].append(m.data)

        # TODO: Fix. There can be different numbers of measurements per wavelength, especially after some filtering operations
        num_slices = len(self.measurements) / len(wavelengths)
        time_slices = [{w : d_by_w[w][i] for w in wavelengths} for i in range(num_slices)]; # A list of dictionaries (one dict per time point)
        time_slices = map(lambda x : pandas.Panel.fromDict(x), time_slices) # Convert the dictionaries into a Pandas Panel.
        self._data = pandas.Panel4D.fromDict({i : time_slices[i] for i in range(len(time_slices))}) # Convert to a 4D Panel.

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value
        if len(self._data.shape) > 4:
            raise TypeError('Plate container was designed only for maximum 4d data sets in mind.')

    def _filter(self, criteria, applyto='measurement', ID=None, copy=False):
        """
        NOT IMPLEMENTED
        Returns a plate container with measurements that only meet the requested criteria.

        Parameters
        ----------
        criteria : callable
            Returns bool.
        applyto : 'measurement'
             'measurement' : criteria is applied to PRMeasurement objects
        ID : str
            ID of the filtered container.
            If None use current sample ID.
        copy : boolean
            If True, a copy of each measurement will be created.
        Returns
        -------
        Filtered container
        """
        raise NotImplementedError

        if ID is None:
            ID = '{0}'.format(self.ID)

        measurements = filter(criteria, self.measurements)

        return self.__class__.from_measurements(ID, measurements, self.meta)

    def apply(self, callable, dtype=float, applyto='well'):
        """
        Applies the callable function to each well and returns a DataFrame as an output.

        Parameters
        -----------------

        callable : func | a callable
            A function that takes a Well instance as an input and returns a calculation.

        dtype : str | int | float | etc.
            Specifies the type of the output return by the callable function.

        applyto : 'Well'
            Only this option is supported at the moment.

        Returns
        ---------------
        DataFrame containing the output of the function applied to each well.
        """
        if applyto.lower() == 'well':
            row_num, col_num = self.data.shape[-2:]

            if hasattr(self.data, 'major_axis') and hasattr(self.data, 'minor_axis'):
                index   = self.data.major_axis
                columns = self.data.minor_axis
            else:
                index   = self.data.index
                columns = self.data.columns

            result = pandas.DataFrame(numpy.nan * numpy.zeros((row_num, col_num)), dtype=dtype, index=index, columns=columns)

            for row in index:
                for col in columns:
                    index = row + col
                    result.ix[row, col] = callable(self[row+col])
        else:
            raise NotImplementedError
        return result

    def __str__(self):
        mystr = '{0} ({1})'.format(self.ID, self.data.shape)
        return mystr

    def iw(self, wavelengths):
        """
        Returns a plate container containing only the indicates wavelengths.

        Parameters
        -------------
        wavelengths : str | list of str

        Returns
        -------------
        Plate container containing only the requested wavelengths
        """
        wavelengths = to_list(wavelengths)

        new = self.copy()
        data = self.data.ix[:, wavelengths]

        if list(data.shape[:2]) == [1, 1]:
            # Fix the plot function here. There's no need to switch object types.
            w = data.items[0]
            return PRMeasurement(w, data.squeeze())
        else:
            new.data = data
            return new

    def plot(self, what=None, xlim='auto', ylim='auto',
                row_labels='auto', col_labels='auto',
                legend=False,
                **kwargs):
        """
        Generalized plot function for the plate container.

        what : None, str | list of str | callable
            Specifies what to plot

        legend : bool
            Not implemented. Keep False.

        xlim, ylim : controls the limits of the plot

        row_labels : list of str

        col_labels : list of str


        TODO: Fix behavior
        other issues with the pandas plot.
        TODO: Fix legend to appear only once on the side
        """
        if hasattr(what, '__call__'):
            callable = what
        elif self.data.shape[0] > 1: # Time data is present
            def f(columns):
                def g(data_slice, **kwargs):
                    return data_slice[columns].plot(ax=pl.gca(), legend=legend, **kwargs)
                return g
            if what is not None:
                callable = f(what)
            else:
                callable = f(self.data.items)
        else:
            raise Exception('Plotting for data of shape {0} not yet implemented. Try plotting individual wavelengths instead.'.format(self.data.shape))

        return graph.plot_ndpanel(self.data, callable,
                xlim=xlim, ylim=ylim, row_labels=row_labels, col_labels=col_labels,
                **kwargs);

