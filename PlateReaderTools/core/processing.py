#!/usr/bin/env python

def remove_baseline_noise(OD, ODbackground = 0.00):
    """ Removes the background noise from the plate reader data. The background noise is approximately ~0.04. """
    return OD - ODbackground

def correct_saturation_OD(OD, g = 0.0): # 0.52 is from a few fits I did a while ago.
    """ Corrects the measured OD to the actual OD. At high cell densities, the measurement on the plate reader saturates. """
    return -1/g * numpy.log(1 - OD * g)
