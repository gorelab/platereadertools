'''
Created on July 20, 2013
@author: Eugene Yurtsev
@email: eyurtsev@gmail.com
'''
import unittest
import numpy
from numpy import array
import glob

from PlateReaderTools.IO import varioskan, infinite, sunrise

class TestParsers(unittest.TestCase):
    def load_files(self, parser, path):
        file_list =  glob.glob(path)

        self.assertNotEqual(len(file_list), 0, 'No files were found')

        #for file_name in file_list:
            #parser.parse_file(file_name)

    def test_varioskan_parser_API(self):
        """ Loading test varioskan data files to check API """
        self.load_files(varioskan, './data/Varioskan/*.*')

    def test_infinite_parser_API(self):
        """ Loading test infinite data files to check API """
        self.load_files(infinite, './data/Infinite/*.*')

    def test_sunrise_parser_API(self):
        """ Loading test sunrise data files to check API """
        self.load_files(sunrise, './data/Sunrise/*.*')

    def test_varioskan_values(self):
        correct_output = array([[ 6.072,  5.925,  5.983,  5.994,  7.161,  3.511,  3.452,  3.563,
                                  3.449,  3.303,  3.448,  3.535],
                                [ 7.565,  5.944,  7.027,  5.978,  3.349,  3.598,  3.644,  3.579,
                                  3.29 ,  3.686,  3.612,  3.359],
                                [ 3.092,  3.077,  3.228,  3.097,  3.46 ,  3.632,  3.312,  3.385,
                                  3.486,  3.341,  3.419,  3.411],
                                [ 3.121,  3.093,  3.237,  3.028,  3.278,  3.766,  3.48 ,  3.572,
                                  3.273,  3.119,  3.72 ,  3.597],
                                [ 2.888,  3.089,  3.332,  3.023,  3.027,  2.746,  3.068,  2.919,
                                  2.912,  3.332,  3.165,  3.341],
                                [ 3.181,  3.113,  3.199,  2.872,  3.158,  3.006,  3.199,  3.039,
                                  3.363,  3.495,  3.236,  3.543],
                                [ 2.943,  3.267,  3.174,  3.171,  2.872,  2.857,  3.164,  3.073,
                                  3.329,  3.332,  3.364,  3.526],
                                [ 3.169,  3.185,  3.298,  3.264,  2.85 ,  3.894,  3.284,  3.453,
                                  3.005,  3.394,  3.276,  3.802]])

        path = './data/Varioskan/Format_01.txt'
        identical= varioskan.parse_file(path)[1][0]['data'] == correct_output
        self.assertFalse(any(~identical.flatten()))


if __name__ == '__main__':
    import nose
    nose.runmodule(argv=[__file__,'-vvs','-x'],
                   exit=False)
